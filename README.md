# package-sets

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [package-sets](#package-sets)
    - [Usage](#usage)
- [The UNIX Man Updates](#the-unix-man-updates)

<!-- markdown-toc end -->

## Usage

To learn how to use and contribute to package sets, please see the
documentation in the [psc-package repository](https://github.com/purescript/psc-package#add-a-package-to-the-package-set)

# The UNIX Man Updates

This also contains packages that are [maintained by or branched from
others]((https://gitlab.com/theunixman/purescript/package-sets-packages)) by [The UNIX Man](https://theunixman.com/) until they can be submitted to [Pursuit](https://pursuit.purescript.org/) or the
various upstream repositories.
